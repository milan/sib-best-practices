class FastaRecord(object):
    """A record in a FASTA file. Consists of a title and a sequence."""
    def __init__(self, title, sequence):
        self.title = title
        self.sequence = sequence

def read_fasta_record(infile):
    """
    Read one record from a FASTA file and return it as a FastaRecord. 
    
    Read from the infile, advancing it, to parse the next record. If no more
    records are in the infile, return None.
    """
    # The first line in a FASTA record is the title line.
    # Examples:
    # >third sequence record
    # >gi|2765657|emb|Z78532.1|CCZ78532 C.californicum 5.8S rRNA gene
    line = infile.readline()

    if not line:
        # End of file
        return None

    # Double-check that it's a FASTA file by looking for the '>'.
    if not line.startswith(">"):
        raise TypeError("Not a FASTA file: %r" % line)

    # The title starts after the '>' and doesn't include any
    # trailing whitespace.
    title = line[1:].rstrip()

    # Read the sequence lines up to the blank line.
    sequence_lines = []
    while True:
        # I know I'm at the end of the sequence lines when I reach the
        # blank line or the end of the file.  I can simplify the test
        # by calling rstrip and checking for the empty string.  If it's
        # a sequence line I'll still call rstrip to remove the final
        # newline and any trailing whitespace so I'll go ahead and
        # always rstring the line.
        line = infile.readline().rstrip()
        if line == "":
            # Reached the end of the record or end of the file
            break
        sequence_lines.append(line)

    # Merge the lines together to make a single string containing the sequence
    # (This is faster than doing "sequence = sequence + line" if there are
    # more than a few lines)
    sequence = "".join(sequence_lines)
    
    return FastaRecord(title, sequence)

def read_fasta_records(input_file):
    """Read all records from a FASTA file and return the records."""
    records = []
    while True:
        record = read_fasta_record(input_file)
        if record is None:
            break
        records.append(record)
    return records


##### Self-test code
import unittest

test_data = [
    ("first sequence record",
     "TACGAGAATAATTTCTCATCATCCAGCTTTAACACAAAATTCGCA"),
    ("second sequence record",
     "CAGTTTTCGTTAAGAGAACTTAACATTTTCTTATGACGTAAATGA" +
     "AGTTTATATATAAATTTCCTTTTTATTGGA"),
    ("third sequence record",
     "GAACTTAACATTTTCTTATGACGTAAATGAAGTTTATATATAAATTTCCTTTTTATTGGA" +
     "TAATATGCCTATGCCGCATAATTTTTATATCTTTCTCCTAACAAAACATTCGCTTGTAAA"),
]

class TestFastaRecord(unittest.TestCase):

    def setUp(self):
        self.infile = open("Data/simple.fasta")

    def test_single_record(self):
        """Test the ability to read a single record at a time"""
        for (title, sequence) in test_data:
            record = read_fasta_record(self.infile)
            if title != record.title:
                raise AssertionError("title %r != %r" % (title, record.title))
            if sequence != record.sequence:
                raise AssertionError("sequence %r != %r" % (sequence,
                                                            record.sequence))
        record = read_fasta_record(self.infile)
        if record is not None:
            raise AssertionError(repr(record))
    
    def test_records(self):
        """Test the ability to read all the records into a list"""
        count = 0
        for record in read_fasta_records(self.infile):
            title, sequence = test_data[count]
            if title != record.title:
                raise AssertionError("title %r != %r" % (title, record.title))
            if sequence != record.sequence:
                raise AssertionError("sequence %r != %r" % (sequence,
                                                            record.sequence))
            count = count + 1

        if count != len(test_data):
            raise AssertionError("Read %d records, expected %d" %
                                 (count, len(test_data)))

if __name__ == '__main__':
    unittest.main()

