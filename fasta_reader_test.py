
import os.path
import shutil
import unittest
from fasta_reader import read_fasta_record, read_fasta_records

class FastaReaderTest(unittest.TestCase):
    def setUp(self):
        """Creates a test folder and if necessary removes a previous version of the test folder.
        """
        self.test_data_folder = "test-data"
        if os.path.exists(self.test_data_folder):
            shutil.rmtree(self.test_data_folder, ignore_errors=True)
        os.mkdir(self.test_data_folder);
        
    def tearDown(self):
        """Closes test file and removes test folder."""
        if self.fasta_file != None:
            self.fasta_file.close()
        shutil.rmtree(self.test_data_folder, ignore_errors=True)

    def test_read_fasta_record(self):
        test_file = self.create_test_file((">first", "GATTACA", "", ">second", "ATTACA"));
        
        read_fasta_record(test_file)
        record = read_fasta_record(test_file)
        
        self.assertEqual("second", record.title)
        self.assertEqual("ATTACA", record.sequence)
    
    def test_read_fasta_records(self):
        test_file = self.create_test_file((">first", "GATTACA", "GGGTA", "", ">second", "ATTACA"));
        
        records = read_fasta_records(test_file)
        
        self.assertEqual("first", records[0].title)
        self.assertEqual("GATTACAGGGTA", records[0].sequence)
        self.assertEqual("second", records[1].title)
        self.assertEqual("ATTACA", records[1].sequence)
        self.assertEqual(2, len(records))
    
    def test_read_fasta_record_with_empty_sequence(self):
        test_file = self.create_test_file((">first", ""));
        
        record = read_fasta_record(test_file)
        
        self.assertEqual("first", record.title)
        self.assertEqual("", record.sequence)
    
    def test_read_empty_file(self):
        test_file = self.create_test_file(());
        
        record = read_fasta_record(test_file)
        
        self.assertEqual(None, record)
    
    def test_read_invalid_fasta_record(self):
        test_file = self.create_test_file(("first", "GATTACA"));
        
        try:
            read_fasta_record(test_file)
        except TypeError as e:
            self.assertEqual("Not a FASTA file: 'first\\n'", e.args[0])
    
    def create_test_file(self, fasta_file_lines):
        """Creates and returns a test file filled with the lines provided by the list 
        'fasta_file_lines'.
        """
        test_data_file = os.path.join(self.test_data_folder, "test")
        fasta_file = open(test_data_file, "w")
        
        try:
            for line in fasta_file_lines:
                fasta_file.write(line)
                fasta_file.write('\n')
        finally:
            fasta_file.close()
        self.fasta_file = open(test_data_file)
        return self.fasta_file

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()